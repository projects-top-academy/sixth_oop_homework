﻿/*

Написать функцию, принимающую в качестве параметров две строки и возвращающую копию первого параметра,
все вхождения второго параметра в которой взяты в <<()>>

"abaracadabra" и "ab",

то вернуть надо

"(ab)aracad(ab)ra".

*/

#include <iostream>
#include <string>

std::string copyStringWithParentheses(const std::string& str, const std::string& subStr) {
    std::string result = str;
    size_t pos = 0;

    while ((pos = result.find(subStr, pos)) != std::string::npos)
    {
        result.replace(pos, subStr.length(), "(" + subStr + ")");
        pos += 2 + subStr.length();
    }
    return result;
}

int main()
{
    std::string str = "abaracadabra";
    std::string subStr = "ab";

    std::string result = copyStringWithParentheses(str, subStr);

    std::cout << result << "\n";
    
    return 0;
}
